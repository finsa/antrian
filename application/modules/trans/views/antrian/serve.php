<div id="modal-data" class="modal-dialog modal-md" role="document">
	<div class="modal-content bg-success">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
        <div class="modal-body p-0">
            <div class="mb-0 form-message text-center"></div>
            <div class="alert alert-success mb-0" style="text-align:center">
            <h5>
                <b><?=$data->var_loket?></b><br>
                <?=idn_date($data->dt_antrian, 'j F Y H.i.s')?><br>
            </h5>
            <h1 style="font-size:120px;font-weight:bold"><?=$data->var_no_antrian?></h1>

            </div>
        </div>
	</div>
</div>

<script>
$(document).ready(function(){
    dataTable.draw();

    setTimeout(function(){
        $modal.modal('hide');
    }, 3000);
});
</script>