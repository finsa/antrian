<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                <div class="form-message text-center"></div>
                <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group row text-sm mb-0">
                                    <label for="loket_filter" class="col-md-3 col-form-label">Loket</label>
                                    <div class="col-md-9">
                                        <select id="loket_filter" name="loket_filter" class="form-control form-control-sm loket_filter" style="width: 100%;">
                                            <?php if ($loket_id == 0){?>
                                            <option value="0">- Semua Loket -</option>
                                            <?php } ?>
                                            <?php
                                            foreach($loket as $lk){
                                                echo '<option value="'.$lk->int_loket_id.'">'.$lk->var_loket.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-group-sm row text-sm mb-0">
									<label for="date_filter" class="col-md-4 col-form-label">Tanggal</label>
									<div class="col-md-8">
										<input type="text" name="date_filter" class="form-control form-control-sm text-right tanggal_filter date_picker">
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>NIK</th>
                            <th>No. Antrian</th>
                            <th>Jam Daftar</th>
                            <th>Pelayanan</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
