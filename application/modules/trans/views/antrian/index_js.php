<script>
    var dataTable = $('#table_data').DataTable({
                "bFilter": false,
                "bServerSide": true,
                "bAutoWidth": false,
                "bProcessing": false,
                "paging":   false,
                "searching": false,
                "ordering": false,
                "info": false,
                "ajax": {
                    "url": "<?=site_url("{$routeURL}") ?>",
                    "dataType": "json",
                    "type": "POST",
                    "data": function(d) {
                        d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                        d.loket_filter = $('.loket_filter').val();
                        d.tanggal_filter = $('.tanggal_filter').val();

                    },
                    "dataSrc": function(json) {
                        if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                        return json.aaData;
                    }
                },
                "aoColumns": [{
                        "sWidth": "20",
                        "sClass":"text-right",
                        "bSearchable": false,
                        "bSortable": false
                    },
                    {
                        "sWidth": "auto",
                        "sClass":"text-right"
                    },
                    {
                        "sWidth": "auto",
                        "sClass":"text-right"
                    },
                    {
                        "sWidth": "auto",
                        "sClass":"text-right"
                    },
                    {
                        "sWidth": "auto",
                    },
                    {
                        "sWidth": "auto"
                    },
                    {
                        "sWidth": "150",
                        "bSearchable": false,
                        "bSortable": false
                    }
                ],
                "aoColumnDefs": [
                {
                    "aTargets": [5],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '<span class="badge bg-danger">Antri</span>';
                                break;
                            case 1:
                                return '<span class="badge bg-success">Selesai</span>';
                                break;
                        }
                    }
                }
            ]
            });
            
    $(document).ready(function() {
        $('.loket_filter').select2();
		$('.date_picker').daterangepicker(datepickModal);

        setInterval(function(){
            if($('.loket_filter').val() != "0"){
                dataTable.draw();
            }else{
                formmsg = {};
                formmsg.stat = false;
                formmsg.msg = "Pilih Loket Terlebih Dahulu";
                setFormMessage('.form-message', formmsg);
            }
        }, 3000);

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
                if($('.loket_filter').val() != "0"){
                    dataTable.search($(this).val()).draw();
                }else{
                    formmsg = {};
                    formmsg.stat = false;
                    formmsg.msg = "Pilih Loket Terlebih Dahulu";
                    setFormMessage('.form-message', formmsg);
                }
			}
        });
        $('.loket_filter, .tanggal_filter').change(function(){
            if($('.loket_filter').val() != "0"){
                dataTable.draw();
            }else{
                formmsg = {};
                formmsg.stat = false;
                formmsg.msg = "Pilih Loket Terlebih Dahulu";
                setFormMessage('.form-message', formmsg);
            }
        });
    });
</script>