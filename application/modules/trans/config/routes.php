<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['antrian']['get']                    = 'trans/antrian';
$route['antrian']['post']                   = 'trans/antrian/list';

$route['antrian/panggil/([a-zA-Z0-9]+)/(:any)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/(:any)']['get']  = 'trans/antrian/call/$1/$2/$3/$4/$5';
$route['antrian/layani/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)']['get']  = 'trans/antrian/serve/$1/$2/$3';