<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use GuzzleHttp\Client;
	
class Antrian extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'antrian'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'trans';
		$this->routeURL = 'antrian';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('antrian_model', 'model');
		$this->load->model('master/loket_model', 'loket');
    }
	
	public function index(){
		//$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Antrian';
		$this->page->menu = 'antrian';
		//$this->page->submenu1 = 'antrian';
		$this->breadcrumb->title = 'Data Antrian';
		$this->breadcrumb->card_title = 'Data Antrian';
		$this->breadcrumb->icon = 'fas fa-users';
		$this->breadcrumb->list = ['Data Antrian'];
		$this->js = true;
		$data['loket_id'] = $this->session->userdata['loket_id'];
		$data['loket'] = $this->loket->get_list($this->session->userdata['loket_id']);
		//$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('antrian/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		//$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('loket_filter', true), $this->input_post('tanggal_filter', true));

		$i 	   = 0;
		foreach($ldata as $d){
			$i++;
			$action = '<div style="width:170px">
						<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/panggil/").$d->int_antrian_id.'/'.$d->var_no_antrian.'/'.$d->int_pelayanan_id.'/'.$this->input_post('loket_filter').'/'.idn_date($d->dt_antrian,'Y-m-d').'" class="ajax_modal btn btn-sm btn-warning tooltips" data-placement="top" data-original-title="Panggil" ><i class="fa fa-volume-up"></i> Panggil</a>
						
						<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/layani/").$d->int_antrian_id.'/'.$this->input_post('loket_filter').'/'.$d->int_loket_terusan_id.'" class="ajax_modal btn btn-sn btn-success tooltips" data-placement="top" data-original-title="Dilayani" ><i class="fa fa-check"></i> Layani</a>
						</div>';
			$data[] = array($i, $d->var_nik, $d->var_no_antrian, idn_date($d->dt_antrian, "H:i:s"), $d->var_pelayanan, $d->int_status_antrian, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								//'iTotalRecords' => $total,
								//'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}
	
	public function call($int_antrian_id, $var_no_antrian, $int_pelayanan_id, $int_loket_id, $dt_antrian){
		if($this->authCheckDetailAccess('r', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->call($int_antrian_id, $var_no_antrian, $int_pelayanan_id, $int_loket_id, $dt_antrian);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$this->led_display($res->var_ip_led, $res->var_no_antrian);
			//$data['call_url'] 	= 'http://'.$res->var_ip_led.'/setText?TextContents='.$var_no_antrian.'    1';
			$data['data'] 	= $res;
			$data['title']	= 'Memanggil Antrian';
			$this->load_view('antrian/call', $data);
		}		
	}

	public function serve($int_antrian_id, $int_loket_id, $int_loket_terusan_id){
		if($this->authCheckDetailAccess('r', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->serve($int_antrian_id, $int_loket_id, $int_loket_terusan_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['title']	= 'Antrian Telah Dilayani';
			$this->load_view('antrian/serve', $data);
		}		
	}
	
	function led_display($var_ip_led, $var_no_antrian){
		
		$url = 'http://'.$var_ip_led.'/setText?TextContents='.$var_no_antrian."    1";

		$ch = curl_init(); 

		curl_setopt_array($ch, [ 
			CURLOPT_RETURNTRANSFER  => true, 
			CURLOPT_HTTPGET         => true,
			CURLOPT_CONNECTTIMEOUT  => 5,
			CURLOPT_URL             => $url 
		]); 
			
		$return = curl_exec($ch); 
		curl_close($ch); 
		/*
		$url = 'http://'.$var_ip_led.'/setText?TextContents='.$var_no_antrian."    1";
        $client = new GuzzleHttp\Client();
        $resp_exec = $client->requestAsync('GET', $url);
        */
       // return $url;
	}
}
