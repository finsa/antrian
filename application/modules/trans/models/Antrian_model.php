<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Antrian_model extends MY_Model {

    public function list($int_loket_id, $dt_antrian){
        return $this->callProcedure("get_data_antrian_perloket(?,?)",
                                    [$int_loket_id, $dt_antrian], 'result');
    }

	public function listCount($filter = NULL){
		$this->db->from($this->m_pelayanan);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_pelayanan', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

    public function call($int_antrian_id, $var_no_antrian, $int_pelayanan_id, $int_loket_id, $dt_antrian){
		//$antrian = $this->get($int_antrian_id, $int_loket_id);
		//$antrian = '';
		//$antrian->var_no_antrian = $var_no_antrian;
		//$antrian->dt_antrian = $dt_antrian;
        
		$call_sp = $this->callProcedure("loket_panggil_antrian(?,?,?,?,?)",
                                    	[$int_antrian_id, $var_no_antrian, $int_pelayanan_id, $int_loket_id, $dt_antrian]);
		if($call_sp){
			$antrian = $this->get($int_antrian_id, $int_loket_id);
			return $antrian;
		}else{
			return false;
		}
    }
	
	function get($int_antrian_id, $int_loket_id){
		$this->db->select("ta.*, ml.*")
			->from($this->t_antrian.' ta')
			->join($this->m_loket." ml", "ml.int_loket_id = ta.int_loket_id", "left")
			->where('ta.int_antrian_id', $int_antrian_id)
			->where('ta.int_loket_id', $int_loket_id);
			
		return $this->db->get()->row();
	}

	public function serve($int_antrian_id, $int_loket_id, $int_loket_terusan_id){
        $serve = $this->callProcedure("loket_set_antrian_selesai(?,?,?)",
                                    [$int_antrian_id, $int_loket_id, $int_loket_terusan_id]);
		if ($serve){
			return $this->get($int_antrian_id, $int_loket_id);
		}else{
			return false;
		}
    }
}
