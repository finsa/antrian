<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends MX_Controller {

	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'MASTER-WILAYAH';
        $this->module   = 'sistem';
        $this->routeURL = 'master_wilayah';

		$this->load->model('wilayah_model', 'model');
        $this->config->set_item('compress_output', FALSE);
    }
	
    public function index(){
		return null;
	}
	public function get_kelurahan(){
		$data = $this->model->get_kelurahan();
        if(empty($data)){
		    $this->set_json(array( 'stat' => false, 
								'data' => NULL,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                'token' => $this->getCsrfToken()]));
        }else{
            $this->set_json(array( 'stat' => TRUE, 
                                    'data' => $data,
                                    'csrf' => [ 'name' => $this->getCsrfName(),
                                    'token' => $this->getCsrfToken()]));
        }

	}


}
