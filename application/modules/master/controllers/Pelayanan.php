<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Pelayanan extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'pelayanan'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'pelayanan';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('pelayanan_model', 'model');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Daftar Pelayanan';
		$this->page->menu = 'master';
		$this->page->submenu1 = 'pelayanan';
		$this->breadcrumb->title = 'Daftar Pelayanan';
		$this->breadcrumb->card_title = 'Daftar Pelayanan';
		$this->breadcrumb->icon = 'fas fa-house-user';
		$this->breadcrumb->list = ['Data Induk', 'Pelayanan'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('pelayanan/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_pelayanan, $d->var_singkatan, $d->var_prefix, $d->var_color, $d->int_pelayanan_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Tambah pelayanan';
		$this->load_view('pelayanan/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('var_pelayanan', 'Nama pelayanan', 'required|min_length[3]|max_length[200]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => true, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $this->model->create($this->input_post());
			$this->set_json([  'stat' => true, 
								'mc' => false, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_pelayanan_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_pelayanan_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_pelayanan_id");
			$data['title']	= 'Edit Tag';
			$this->load_view('pelayanan/index_action', $data);
		}
		
	}

	public function update($int_pelayanan_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('var_pelayanan', 'Nama pelayanan', 'required|min_length[3]|max_length[200]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($int_pelayanan_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($int_pelayanan_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_pelayanan_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_pelayanan_id/del");
			$data['title']	= 'Hapus Data pelayanan';
			$data['info']   = [ 'Nama pelayanan' => $res->var_pelayanan];
			$this->load_view('pelayanan/index_delete', $data);
		}
	}

	public function delete($int_pelayanan_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_pelayanan_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
