<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Loket extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'loket'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'loket';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('loket_model', 'model');
		$this->load->model('pelayanan_model', 'pelayanan');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Loket';
		$this->page->menu = 'master';
		$this->page->submenu1 = 'loket';
		$this->breadcrumb->title = 'Data Loket';
		$this->breadcrumb->card_title = 'Data Loket';
		$this->breadcrumb->icon = 'fas fa-chalkboard-teacher';
		$this->breadcrumb->list = ['Data Induk', 'Loket'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('loket/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_loket, $d->var_pelayanan, $d->var_ip_led, $d->var_loket_terusan, $d->is_aktif, $d->int_loket_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Tambah loket';
		$data['pelayanan']      = $this->pelayanan->get_list();
		$data['loket']			= $this->model->get_list();
		$this->load_view('loket/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('var_loket', 'Nama loket', 'required|min_length[3]|max_length[200]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => true, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $this->model->create($this->input_post());
			$this->set_json([  'stat' => true, 
								'mc' => false, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_loket_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_loket_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_loket_id");
			$data['title']	= 'Ubah Loket';
			$data['pelayanan']	= $this->pelayanan->get_list();
			$data['loket']		= $this->model->get_list();
			$this->load_view('loket/index_action', $data);
		}
		
	}

	public function update($int_loket_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('var_loket', 'Nama loket', 'required|min_length[3]|max_length[200]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($int_loket_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Updated Successfully" : "Data Update Failed",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($int_loket_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_loket_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_loket_id/del");
			$data['title']	= 'Hapus Data loket';
			$data['info']   = [ 'Nama loket' => $res->var_loket];
			$this->load_view('loket/index_delete', $data);
		}
	}

	public function delete($int_loket_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_loket_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
