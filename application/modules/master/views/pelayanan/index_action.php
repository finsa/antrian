<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="data_form" width="80%">
<div id="modal-form" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="var_pelayanan" class="col-sm-4 col-form-label">Nama pelayanan</label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_pelayanan" placeholder="Nama pelayanan" name="var_pelayanan" value="<?=isset($data->var_pelayanan)? $data->var_pelayanan : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_singkatan" class="col-sm-4 col-form-label">Singkatan</i></label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_singkatan" placeholder="Singkatan" name="var_singkatan" value="<?=isset($data->var_singkatan)? $data->var_singkatan : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_prefix" class="col-sm-4 col-form-label">Prefix</label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_prefix" placeholder="Prefix" name="var_prefix" value="<?=isset($data->var_prefix)? $data->var_prefix : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_color" class="col-sm-4 col-form-label">Warna</label>
				<div class="col-sm-8">
					<select id="var_color" name="var_color" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="">- Pilih -</option>
						<option value="danger">Merah</option>
						<option value="warning">Kuning</option>
						<option value="success">Hijau</option>
						<option value="primary">Biru</option>
						<option value="secondary">Abu-abu</option>
						<option value="dark">Hitam</option>
					</select>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.select2').select2();

		$("#data_form").validate({
			rules: {
			    var_pelayanan:{
			        required: true,
					minlength: 2,
					maxlength: 200
				},
			    var_singkatan:{
			        required: true,
					minlength: 2,
					maxlength: 100
				},
			    var_prefix:{
			        required: true,
					minlength: 1,
					maxlength: 1
				},
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-form';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#data_form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>