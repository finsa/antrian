<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="data_form" width="80%">
<div id="modal-form" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="int_pelayanan_id" class="col-sm-4 col-form-label">Nama Pelayanan</label>
				<div class="col-sm-8">
					<select id="int_pelayanan_id" name="int_pelayanan_id" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="">- Pilih -</option>
						<?php 
							foreach($pelayanan as $pl){
								echo '<option value="'.$pl->int_pelayanan_id.'">'.$pl->var_pelayanan.'</option>';
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_loket" class="col-sm-4 col-form-label">Nama Loket</label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_loket" placeholder="Nama Loket" name="var_loket" value="<?=isset($data->var_loket)? $data->var_loket : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_singkatan" class="col-sm-4 col-form-label">Singkatan</label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_singkatan" placeholder="Singkatan" name="var_singkatan" value="<?=isset($data->var_singkatan)? $data->var_singkatan : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_ip_led" class="col-sm-4 col-form-label">IP LED</label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_ip_led" placeholder="IP LED" name="var_ip_led" value="<?=isset($data->var_ip_led)? $data->var_ip_led : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_suara_antrian" class="col-sm-4 col-form-label">Panggilan</label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_suara_antrian" placeholder="Suara Panggilan Antrian" name="var_suara_antrian" value="<?=isset($data->var_suara_antrian)? $data->var_suara_antrian : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_color" class="col-sm-4 col-form-label">Warna</label>
				<div class="col-sm-8">
					<select id="var_color" name="var_color" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="">- Pilih -</option>
						<option value="danger">Merah</option>
						<option value="warning">Kuning</option>
						<option value="success">Hijau</option>
						<option value="primary">Biru</option>
						<option value="secondary">Abu-abu</option>
						<option value="dark">Hitam</option>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="int_loket_terusan_id" class="col-sm-4 col-form-label">Loket Lanjutan</label>
				<div class="col-sm-8">
					<select id="int_loket_terusan_id" name="int_loket_terusan_id" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="">- Pilih Loket-</option>
						<?php 
							foreach($loket as $lk){
								echo '<option value="'.$lk->int_loket_id.'">'.$lk->var_loket.'</option>';
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="Status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-10 mt-1">
					<div class="icheck-primary d-inline mr-2">
						<input type="radio" id="radioPrimary1" name="is_aktif" value="1" <?=isset($data->is_aktif)? (($data->is_aktif == 1)? 'checked' : '') : 'checked' ?>>
							<label for="radioPrimary1">Aktif </label>
					</div>
					<div class="icheck-danger d-inline">
						<input type="radio" id="radioPrimary2" name="is_aktif" value="0" <?=isset($data->is_aktif)? (($data->is_aktif == 0)? 'checked' : '') : '' ?>>
						<label for="radioPrimary2">Non-aktif</label>
					</div>
				</div>
			</div>		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.select2').select2();
		<?php if(isset($data->int_pelayanan_id)) echo '$("#int_pelayanan_id").val("'.$data->int_pelayanan_id.'").trigger("change");'?>
		<?php if(isset($data->int_loket_terusan_id)) echo '$("#int_loket_terusan_id").val("'.$data->int_loket_terusan_id.'").trigger("change");'?>
		<?php if(isset($data->var_color)) echo '$("#var_color").val("'.$data->var_color.'").trigger("change");'?>

		$("#data_form").validate({
			rules: {
			    int_pelayanan_id:{
			        required: true
				},
			    var_loket:{
			        required: true,
					minlength: 2,
					maxlength: 20
				},
			    var_singkatan:{
			        required: true,
					digits: true,
					minlength: 2,
					maxlength: 2
				},
			    var_ip_led:{
			        required: true,
					minlength: 7,
					maxlength: 15
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-form';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#data_form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>