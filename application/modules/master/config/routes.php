<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['loket']['get']          			    = 'master/loket';
$route['loket']['post']         			    = 'master/loket/list';
$route['loket/add']['get']      			    = 'master/loket/add';
$route['loket/save']['post']    			    = 'master/loket/save';
$route['loket/([a-zA-Z0-9]+)']['get']           = 'master/loket/get/$1';
$route['loket/([a-zA-Z0-9]+)']['post']          = 'master/loket/update/$1';
$route['loket/([a-zA-Z0-9]+)/del']['get']       = 'master/loket/confirm/$1';
$route['loket/([a-zA-Z0-9]+)/del']['post']      = 'master/loket/delete/$1';

$route['pelayanan']['get']                      = 'master/pelayanan';
$route['pelayanan']['post']                     = 'master/pelayanan/list';
$route['pelayanan/add']['get']                  = 'master/pelayanan/add';
$route['pelayanan/save']['post']                = 'master/pelayanan/save';
$route['pelayanan/([a-zA-Z0-9]+)']['get']       = 'master/pelayanan/get/$1';
$route['pelayanan/([a-zA-Z0-9]+)']['post']      = 'master/pelayanan/update/$1';
$route['pelayanan/([a-zA-Z0-9]+)/del']['get']   = 'master/pelayanan/confirm/$1';
$route['pelayanan/([a-zA-Z0-9]+)/del']['post']  = 'master/pelayanan/delete/$1';

$route['wilayah/kelurahan']['post']                 = 'master/wilayah/get_kelurahan';