<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pelayanan_model extends MY_Model {

	public function get_list(){
		return $this->db->query("SELECT int_pelayanan_id, var_pelayanan 
								 FROM	{$this->m_pelayanan}
								 ORDER BY int_pelayanan_id ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_pelayanan);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_pelayanan', $filter)
					->group_end();
		}

		$order = 'var_pelayanan ';
		switch($order_by){
			case 1 : $order = 'var_pelayanan '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_pelayanan);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_pelayanan', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		$upd['created_at'] = date("Y-m-d H:i:s");
		$upd['created_by'] = $this->session->userdata['user_id'];
		return $this->db->insert($this->m_pelayanan, $ins);
	}

	public function get($int_pelayanan_id){
		return $this->db->select("*")
					->get_where($this->m_pelayanan, ['int_pelayanan_id' => $int_pelayanan_id])->row();
	}

	public function update($int_pelayanan_id, $upd){
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();

		$this->db->where('int_pelayanan_id', $int_pelayanan_id);
		$this->db->update($this->m_pelayanan, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_pelayanan_id){
		$upd['deleted_at'] = date("Y-m-d H:i:s");
		$upd['deleted_by'] = $this->session->userdata['user_id'];
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_pelayanan_id', $int_pelayanan_id);
		$this->db->update($this->m_pelayanan, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	
}
