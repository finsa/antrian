<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Loket_model extends MY_Model {

	public function get_list($id_loket = ''){
		$this->db->select("int_loket_id, var_loket, var_singkatan, var_color")
					->from($this->m_loket);

		if(!empty($id_loket) || $id_loket != 0){ // filters
            $this->db->where('int_loket_id', $id_loket);
		}

		$order = 'int_loket_id ';
		$sort = 'ASC';
	
		return $this->db->order_by($order, $sort)->get()->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("ml.*, l.var_loket AS var_loket_terusan, mp.var_pelayanan")
					->from($this->m_loket." ml")
					->join($this->m_pelayanan." mp", "mp.int_pelayanan_id = ml.int_pelayanan_id", "left")
					->join($this->m_loket." l", "ml.int_loket_terusan_id = l.int_loket_id", "left");

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('ml.var_loket', $filter)
					->group_end();
		}

		$order = 'ml.var_loket ';
		switch($order_by){
			case 1 : $order = 'ml.var_loket '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_loket);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_loket', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		$upd['created_at'] = date("Y-m-d H:i:s");
		$upd['created_by'] = $this->session->userdata['user_id'];
		return $this->db->insert($this->m_loket, $ins);
	}

	public function get($int_loket_id){
		return $this->db->select("*")
					->get_where($this->m_loket, ['int_loket_id' => $int_loket_id])->row();
	}

	public function update($int_loket_id, $upd){
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();

		$this->db->where('int_loket_id', $int_loket_id);
		$this->db->update($this->m_loket, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_loket_id){
		$upd['deleted_at'] = date("Y-m-d H:i:s");
		$upd['deleted_by'] = $this->session->userdata['user_id'];
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_loket_id', $int_loket_id);
		$this->db->update($this->m_loket, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	
}
