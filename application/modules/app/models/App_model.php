<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class App_model extends MY_Model {

    public function get_akhir_k1s(){
        return $this->callProcedure("dashboard_rekap_k1_expired_kontrak()",'');
    }

    public function get_akhir_k2(){
        return $this->callProcedure("dashboard_rekap_k2_expired_kontrak()",'');
    }

    public function get_akhir_kontrak1(){
		$filter_start_kontrak = date('Y-m-d 00:00:00');
		$filter_end_kontrak = date('Y-m-d 23:59:59', strtotime(date('Y-m-d'). ' + 30 days'));

        $this->db->select("*")
				->from($this->m_karyawan_kontrak." mkk")
				->where("var_status_kerja IN ('KONTRAK 1')")
                ->where('((mkk.dt_tanggal_akhir_k1 BETWEEN "'.$filter_start_kontrak.'" AND "'.$filter_end_kontrak.'") OR (mkk.dt_tanggal_akhir_k2 BETWEEN "'.$filter_start_kontrak.'" AND "'.$filter_end_kontrak.'"))')
				->where("int_status = 1");
		
        return $this->db->count_all_results();
    }

    public function get_akhir_kontrak2(){
		$filter_start_kontrak = date('Y-m-d 00:00:00');
		$filter_end_kontrak = date('Y-m-d 23:59:59', strtotime(date('Y-m-d'). ' + 30 days'));

        $this->db->select("*")
				->from($this->m_karyawan_kontrak." mkk")
				->where("var_status_kerja IN ('KONTRAK 2')")
                ->where('((mkk.dt_tanggal_akhir_k1 BETWEEN "'.$filter_start_kontrak.'" AND "'.$filter_end_kontrak.'") OR (mkk.dt_tanggal_akhir_k2 BETWEEN "'.$filter_start_kontrak.'" AND "'.$filter_end_kontrak.'"))')
				->where("int_status = 1");
		
        return $this->db->count_all_results();
    }

    public function get_tk(){
        return $this->callProcedure("dashboard_rekap_absensi_tk_notif()",'');
    }
}