<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['homepage']['post']              = 'homepage/homepage/data_antrian';
$route['pendaftaran']['get']            = 'homepage/homepage/register';
$route['pendaftaran/pelayanan']['get']  = 'homepage/homepage/service';
$route['ambil_tiket/([0-9]+)']['get']   = 'homepage/homepage/ticket/$1';

$route['print/ticket']['post']          = 'homepage/homepage/print';
