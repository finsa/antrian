<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\CapabilityProfile\SimpleCapabilityProfile;
use Mike42\Escpos\Printer;


class Homepage extends MX_Controller {
	private $input_file_name = 'lampiran';
	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'homepage';
		$this->routeURL = 'homepage';

		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
	
		//$this->load->model('master/kategori_model', 'kategori');
		$this->load->model('master/loket_model', 'loket');
		$this->load->model('homepage_model', 'model');
    }

	public function index(){
		//$this->page->menu = 'dashboard';
		$this->page->subtitle = 'Beranda';
		$this->css = true;
		$this->js = true;

		$data_loket	= $this->loket->get_list();
		$list_loket = '';

		foreach ($data_loket as $dl){
			$var_color = 'dark';
			if($dl->var_color){
				$var_color = $dl->var_color;
			}
			$list_loket .= '<div class="col-sm-4">
								<div class="info-box shadow-lg">
									<span class="info-box-icon bg-'.$var_color.'" style="width: 100px;height: 100px;font-size: 20px;display:block;">
										<div>Loket</div>
										<h2 style="font-size: 70px;font-weight: bold;line-height:0.8;">'.$dl->var_singkatan.'</h2>
									</span>

									<div class="info-box-content">
										<h2 id="antrian_loket_'.$dl->int_loket_id.'" style="font-size: 76px;font-weight: bold;line-height:1;text-align: right;">[] [] [] []</h2>
									</div>
								</div>
							</div>';
		}

		$data['list_loket'] = $list_loket;

		$this->render_view('homepage/index', $data, false, 'public');
	}

	public function register(){
		//$this->page->menu = 'dashboard';
		$this->page->subtitle = 'Pendaftaran';
		//$this->css = true;
		//$this->js = true;

		$list_pengaduan = '';

		$list_informasi = '';


		$data['list_pengaduan'] = $list_pengaduan;
		$data['list_informasi'] = $list_informasi;

		$this->render_view('homepage/register', $data, false, 'public');
	}

	public function service(){
		//$this->page->menu = 'dashboard';
		$this->page->subtitle = 'Daftar Pelayanan';
		//$this->css = true;
		//$this->js = true;

		$get_services = $this->model->get_services_list();

		$url_service = site_url("ambil_tiket");


		$list_service = '';
		foreach ($get_services as $srv){
			$var_color = 'dark';
			if($srv->var_color){
				$var_color = $srv->var_color;
			}
			$list_service .= '<div class="col-sm-6">
								<div class="info-box shadow-lg ajax_modal" data-block="body" data-url="'.$url_service.'/'.$srv->int_pelayanan_id.'" style="cursor:pointer;min-height:100px">
									<span class="info-box-icon bg-'.$var_color.'" style="width: 80px;height: 80px;font-size: 50px;display:block;">
										<i class="fas fa-house-user"></i>
									</span>
									<div class="info-box-content">
										<h4 style="margin:20px 10px">'.$srv->var_pelayanan.'</h4>
									</div>
								</div>
							</div>';
			}

		$data['list_service'] = $list_service;

		$this->render_view('homepage/service', $data, false, 'public');
	}
	
	public function ticket($int_pelayanan_id){	  
		$ticket = $this->model->create_ticket($int_pelayanan_id);
		if(empty($ticket)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $ticket;
			$data['url']	= site_url("print/ticket");
			$data['title']	= 'Pendaftaran Berhasil, Silahkan Ambil Nomor Antrian Anda';
			//$data['pelayanan'] = $this->pelayanan->get_list();
			$this->load_view('homepage/ticket', $data);
			//$this->print($ticket->var_no_antrian, $ticket->var_pelayanan, $ticket->dt_antrian);
		}
	}

	public function data_antrian(){
		$query = $this->model->get_data_antrian(date('Y-m-d 23:59:59'));
		$this->set_json([  'data' => $query, 
							'mc' => $query, //modal close
							'msg' => ($query)? "Data Successfully Loaded" : "Data Not Found"
						]);
	}

	function print(){
		$data_post = $this->input_post();

		$connector = new WindowsPrintConnector($this->config->item('printer_name'));
		$printer = new Printer($connector);
		try {
			$printer->setJustification(Printer::JUSTIFY_CENTER); //
			$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT );
			$printer->text("DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL");
			$printer->feed();
			$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT);
			$printer->text("K O T A   M A L A N G");
			$printer->selectPrintMode();
			$printer->feed(2);
			$printer->selectPrintMode(Printer::MODE_FONT_B);
			$printer->text("".idn_date($this->input_post('dt_antrian'), 'l, j F Y H:i:s')."\n");
			$printer->selectPrintMode();
			$printer->feed();
			$printer->selectPrintMode(Printer::MODE_FONT_A);
			$printer->text("Nomor Antrian :\n");
			$printer->selectPrintMode();
			$printer->feed();
			$printer->selectPrintMode(Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH | Printer::MODE_DOUBLE_WIDTH);
			$printer->setTextSize(5,5);
			$printer->text("".$this->input_post('var_no_antrian')."\n");
			///$printer->setTextSize(1,1);
			$printer->selectPrintMode();
			$printer->feed();
			$printer->selectPrintMode(Printer::MODE_FONT_B);
			$printer->text("Pelayanan :");
			$printer->selectPrintMode();
			$printer->feed();
			$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT);
			$printer->text("".$this->input_post('var_pelayanan')."\n");
			$printer->selectPrintMode();
			$printer->feed();
			$printer->selectPrintMode(Printer::MODE_FONT_B| Printer::MODE_EMPHASIZED);
			$printer->text("...................................");
			$printer->selectPrintMode();
			$printer->feed(1);
			$printer->cut();
			$printer->close();
		} finally {
			$printer -> close();
		}

		return true;
	}
}
