<div class="col-md-2" style="margin-top: -70px;color: #fff;text-align: right;">
	<img src="<?=site_url('assets/images/logo.png')?>" width="100px" height="100px">
</div>
<div class="col-md-10" style="margin-top: -45px;color: #fff;">
	<h1>Dinas Kependudukan dan Pencatatan Sipil <b>Kota Malang</b></h1>
</div>
<div class="col-md-12">
	<div class="card card-danger shadow-none">
		<div class="card-header">
			<h3 class="card-title">Pilih Pelayanan yang Akan Dituju</h3>
		</div>
		<div class="card-body row">
			<?=$list_service?>
		</div>
	</div>
</div>

<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
