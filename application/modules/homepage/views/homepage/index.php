<div class="col-md-2" style="margin-top: -70px;color: #fff;text-align: right;">
	<img src="<?=site_url('assets/images/logo.png')?>" width="100px" height="100px">
</div>
<div class="col-md-10" style="margin-top: -45px;color: #fff;">
	<h1>Dinas Kependudukan dan Pencatatan Sipil <b>Kota Malang</b></h1>
</div>
<div class="col-md-12">
	<div class="card card-warning shadow-none">
		<div class="card-header">
			<h3 class="card-title">Loket Pelayanan</h3>
		</div>
		<div class="card-body row">
			<?=$list_loket?>
		</div>
	</div>
</div>

<div id="menu" style="display: block;position: fixed;bottom: 0;right: 10px;">
		<a href="<?=site_url('pendaftaran/pelayanan')?>" class="btn btn-app">
			<i class="fas fa-receipt"></i> Antrian
		</a>
		<a href="<?=site_url('dashboard')?>" class="btn btn-app">
			<i class="fas fa-chalkboard-teacher"></i> Operator
		</a>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
