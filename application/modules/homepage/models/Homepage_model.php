<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Homepage_model extends MY_Model {

    public function get_services_list($start = 0, $limit = 100){
		$this->db->select("*")
					->from($this->m_pelayanan)
					->order_by('int_pelayanan_id', 'ASC')
					->limit($limit, $start);
		
		return $this->db->get()->result();
    }
    
  public function create_ticket($int_pelayanan_id, $var_nik = ''){
      return $this->callProcedure("generate_no_antrian(?,?)",
                                  [$int_pelayanan_id,$var_nik]);
  }

  public function get_data_antrian($dt_antrian){
        return $this->callProcedure("get_monitoring_antrian_loket(?)",
                                    [$dt_antrian], 'result');
    }
}