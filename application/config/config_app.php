<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['api_prefix']       = 'CMS';
$config['api_suffix']       = '@&#';
$config['api_expire']       = 480;                      // 480 menit
$config['api_user_id']      = "API-User";
$config['api_token_name']   = "API-Token";
$config['api_identifier']   = "API-Identifier";
$config['use_cache']        = true;                     // cache output html
$config['cache_time']       = 480;                      // lama waktu simpan cache. dalam menit
$config['use_minify']       = false;                     // Minify output html, css, js
$config['minify_level']     = 1;                        // 1: simple, 2: one line
$config['use_cdns']         = false;                    // penggunaan cdns
$config['page_title']       = "Antrian Dispendukcapil Kota Malang";
$config['app_name']         = "Antrian Dispendukcapil Kota Malang";
$config['app_alias']        = "Antrian Dispendukcapil Kota Malang";
$config['footer_left']      = "Antrian Dispendukcapil Kota Malang";
$config['footer_right']     = "Antrian Dispendukcapil Kota Malang";
$config['cdn_url']          = "http://localhost/antrian/";
$config['img_dir']          = "assets/images/";
$config['upload_path']      = "assets/images/"; 
$config['allowed_types']    = "jpg|png|jpeg";
$config['printer_name']     = "tm-t82";
$config['encrypt_name']     = true; 
$config['max_size']         = 2048;  