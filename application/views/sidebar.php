<aside class="main-sidebar elevation-4 sidebar-dark-green">
    <!--<a href="#" class="brand-link navbar-green">
        <img src="<?=base_url() ?>assets/images/logo.png" alt="Logo" class="brand-image" style="opacity:1">
        <span class="brand-text font-weight-light"><strong><?=$sidebar->name ?></strong></span>
    </a>-->
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-compact nav-child-indent text-sm" data-widget="treeview" role="menu" data-accordion="false">
                <?=$this->session->userdata('menu')?>
            </ul>
        </nav>
    </div>
</aside>