/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.3.24-MariaDB-log : Database - antrian
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`antrian` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `antrian`;

/*Table structure for table `m_loket` */

DROP TABLE IF EXISTS `m_loket`;

CREATE TABLE `m_loket` (
  `int_loket_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_pelayanan_id` int(11) DEFAULT NULL,
  `var_loket` varchar(100) DEFAULT NULL,
  `var_singkatan` varchar(50) DEFAULT NULL,
  `var_prefix` varchar(10) DEFAULT NULL,
  `var_color` varchar(50) DEFAULT NULL,
  `var_icon` varchar(50) DEFAULT NULL,
  `var_suara_antrian` varchar(50) DEFAULT NULL,
  `var_ip_led` varchar(50) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `int_loket_terusan_id` int(11) DEFAULT 0,
  `created_at` datetime(6) DEFAULT current_timestamp(6),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_loket_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_loket` */

insert  into `m_loket`(`int_loket_id`,`int_pelayanan_id`,`var_loket`,`var_singkatan`,`var_prefix`,`var_color`,`var_icon`,`var_suara_antrian`,`var_ip_led`,`is_aktif`,`int_loket_terusan_id`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,1,'Loket 10','10','A','danger','fas fa-user-md','sepuluh','192.168.43.204',1,2,'2021-07-18 14:27:41.758035',1,'2021-08-14 09:56:54.000000',1,NULL,NULL),
(2,2,'Loket 11','11','B','warning','fas fa-assistive-listening-systems','sebelas','192.168.43.203',1,0,'2021-07-18 14:27:41.876592',1,NULL,NULL,NULL,NULL),
(3,3,'Loket 17','17','C','','fas fa-leaf','tujuh belas','192.168.43.202',1,4,'2021-07-18 14:27:41.978742',1,'2021-08-15 22:16:42.000000',1,NULL,NULL),
(4,3,'Loket 18','18','C','success','fas fa-universal-access','delapan belas','192.168.43.202',1,0,'2021-07-18 14:27:42.088762',1,NULL,NULL,NULL,NULL),
(5,4,'Loket 20','20','D','primary','far fa-smile','dua puluh','192.168.43.201',1,0,'2021-07-18 14:27:42.175706',1,NULL,NULL,NULL,NULL),
(6,5,'Loket 21','21','E','success','fas fa-leaf','dua puluh satu','192.168.43.205',1,0,'2021-07-18 14:27:42.275853',1,NULL,NULL,NULL,NULL),
(7,6,'Loket 22','22','F','warning','fas fa-universal-access','dua puluh dua','192.168.43.206',1,0,'2021-07-18 14:27:42.360652',1,NULL,NULL,NULL,NULL),
(8,7,'Loket 23','23','G','danger','fas fa-assistive-listening-systems','dua puluh tiga','192.168.43.207',1,0,'2021-07-18 14:27:42.455710',1,NULL,NULL,NULL,NULL),
(9,8,'Loket 24','24','H','success','fas fa-leaf','dua puluh empat','192.168.43.208',1,0,'2021-07-18 14:27:42.540753',1,NULL,NULL,NULL,NULL),
(10,9,'Loket 26','26','I','','fas fa-leaf','dua puluh enam','192.168.43.209',1,0,'2021-07-18 14:27:42.625763',1,NULL,NULL,NULL,NULL),
(11,10,'Loket 27','27','J','','fas fa-leaf','dua puluh tujuh','192.168.43.210',1,0,'2021-07-18 14:27:42.625763',1,NULL,NULL,NULL,NULL);

/*Table structure for table `m_pelayanan` */

DROP TABLE IF EXISTS `m_pelayanan`;

CREATE TABLE `m_pelayanan` (
  `int_pelayanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `var_pelayanan` varchar(100) DEFAULT NULL,
  `var_singkatan` varchar(50) DEFAULT NULL,
  `var_prefix` varchar(10) DEFAULT NULL,
  `var_color` varchar(50) DEFAULT NULL,
  `var_icon` varchar(50) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT current_timestamp(6),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_pelayanan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_pelayanan` */

insert  into `m_pelayanan`(`int_pelayanan_id`,`var_pelayanan`,`var_singkatan`,`var_prefix`,`var_color`,`var_icon`,`is_aktif`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,'Caping Akta Kelahiran','Akta Kelahiran','A','danger','fas fa-user-md',1,'2021-07-18 14:28:41.938894',1,NULL,NULL,NULL,NULL),
(2,'Keabsahan Luar Kota','Keabsahan Luar Kota','B','warning','fas fa-assistive-listening-systems',1,'2021-07-18 14:28:42.032088',1,NULL,NULL,NULL,NULL),
(3,'Perubahan Status Anak - Kewarganegaraan - Kematian','Perubahan Status','C','','fas fa-leaf',1,'2021-07-18 14:28:42.121636',1,NULL,NULL,NULL,NULL),
(4,'Pelayanan KK Dan KTP','KK Dan KTP','D','success','fas fa-universal-access',1,'2021-07-18 14:28:42.211491',1,NULL,NULL,NULL,NULL),
(5,'Pencatatan Perkawinan Dan Perceraian','Perkawinan Dan Perceraian','E','primary','far fa-smile',1,'2021-07-18 14:28:42.291764',1,NULL,NULL,NULL,NULL),
(6,'Pelayanan KIA','KIA','F','primary','fas fa-leaf',1,'2021-07-18 14:28:42.371641',1,NULL,NULL,NULL,NULL),
(7,'Pelayanan Legalisir','Legalisir','G','','far fa-smile',1,'2021-07-18 14:28:42.456382',1,NULL,NULL,NULL,NULL),
(8,'Pelayanan Surat Pindan Dan SKTT','Surat Pindan Dan SKTT','H','success','fas fa-universal-access',1,'2021-07-18 14:28:42.547724',1,NULL,NULL,NULL,NULL),
(9,'Pelayanan Pencarian Berkas','Pencarian Berkas','I','warning','fas fa-assistive-listening-systems',1,'2021-07-18 14:28:42.635678',1,NULL,NULL,NULL,NULL),
(10,'Perekaman Dan Cetak KTP Elektronik','Perekaman - Cetak KTP','J','danger','fas fa-user-md',1,'2021-07-18 14:28:42.727643',1,NULL,NULL,NULL,NULL);

/*Table structure for table `m_status_antrian` */

DROP TABLE IF EXISTS `m_status_antrian`;

CREATE TABLE `m_status_antrian` (
  `int_status_antrian` int(11) NOT NULL,
  `var_status_antrian` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`int_status_antrian`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_status_antrian` */

insert  into `m_status_antrian`(`int_status_antrian`,`var_status_antrian`) values 
(0,'Antri'),
(1,'Selesai Dilayani'),
(2,'Dibatalkan'),
(3,'Selesai Dilayani (Loket Lanjutan)');

/*Table structure for table `m_user_admin` */

DROP TABLE IF EXISTS `m_user_admin`;

CREATE TABLE `m_user_admin` (
  `int_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `var_nama_depan` varchar(100) DEFAULT NULL,
  `var_nama_belakang` varchar(100) DEFAULT NULL,
  `var_username` varchar(50) DEFAULT NULL,
  `var_password` varchar(255) DEFAULT NULL,
  `int_group_id` int(11) DEFAULT NULL,
  `int_loket_id` int(11) DEFAULT NULL,
  `int_status` int(11) DEFAULT NULL,
  `dt_registrasi` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`int_user_id`) USING BTREE,
  KEY `intIDuser` (`int_user_id`) USING BTREE,
  KEY `IDX_m_user_admin_intIDGroup` (`int_group_id`) USING BTREE,
  FULLTEXT KEY `IDX_m_user_admin_txtUsername` (`var_username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_user_admin` */

insert  into `m_user_admin`(`int_user_id`,`var_nama_depan`,`var_nama_belakang`,`var_username`,`var_password`,`int_group_id`,`int_loket_id`,`int_status`,`dt_registrasi`) values 
(1,'Super','Admin','super','$2y$10$qTT3fCQD2.R7PwkKpKvHEusSz.nTQyEjnGjePsY3MQiXDM2XB90cq',1,0,1,'2021-07-14 09:14:47');

/*Table structure for table `m_user_admin_group` */

DROP TABLE IF EXISTS `m_user_admin_group`;

CREATE TABLE `m_user_admin_group` (
  `int_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `var_nama` varchar(50) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_user_admin_group` */

insert  into `m_user_admin_group`(`int_group_id`,`var_nama`,`is_active`) values 
(1,'Admin Sistem',1);

/*Table structure for table `m_user_admin_group_menu` */

DROP TABLE IF EXISTS `m_user_admin_group_menu`;

CREATE TABLE `m_user_admin_group_menu` (
  `int_group_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_group_id` int(11) DEFAULT NULL,
  `int_menu_id` int(11) DEFAULT NULL,
  `c` tinyint(4) DEFAULT 0,
  `r` tinyint(4) DEFAULT 0,
  `u` tinyint(4) DEFAULT 0,
  `d` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`int_group_menu_id`) USING BTREE,
  KEY `IDX_m_user_admin_group_menu_intIDGroup` (`int_group_id`) USING BTREE,
  KEY `IDX_m_user_admin_group_menu_intIDMenu` (`int_menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_user_admin_group_menu` */

insert  into `m_user_admin_group_menu`(`int_group_menu_id`,`int_group_id`,`int_menu_id`,`c`,`r`,`u`,`d`) values 
(135,1,1,1,1,1,1),
(136,1,2,1,1,1,1),
(137,1,8,0,1,0,0),
(138,1,81,1,1,1,1),
(139,1,82,1,1,1,1),
(140,1,9,0,1,0,0),
(141,1,93,1,1,1,1),
(142,1,94,1,1,1,1),
(143,1,95,1,1,1,1);

/*Table structure for table `m_user_admin_menu` */

DROP TABLE IF EXISTS `m_user_admin_menu`;

CREATE TABLE `m_user_admin_menu` (
  `int_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `var_kode` varchar(25) DEFAULT NULL,
  `var_nama` varchar(50) DEFAULT NULL,
  `var_url` varchar(255) DEFAULT NULL,
  `int_level` int(11) DEFAULT NULL,
  `int_urutan` int(11) DEFAULT NULL,
  `int_parent_id` int(11) DEFAULT NULL,
  `var_class` varchar(25) DEFAULT NULL,
  `var_icon` varchar(50) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`int_menu_id`) USING BTREE,
  KEY `IDX_m_user_admin_menu_intIdMenu` (`int_menu_id`) USING BTREE,
  KEY `IDX_m_user_admin_menu_intParentId` (`int_parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_user_admin_menu` */

insert  into `m_user_admin_menu`(`int_menu_id`,`var_kode`,`var_nama`,`var_url`,`int_level`,`int_urutan`,`int_parent_id`,`var_class`,`var_icon`,`is_active`) values 
(1,'DASHBOARD','Dashboard','dashboard',1,1,NULL,'dashboard','fas fa-th-large',1),
(2,'ANTRIAN','Antrian','antrian',1,2,NULL,'antrian','fas fa-users',1),
(8,'MASTER','Data Induk',NULL,1,8,NULL,'master','fas fa-database',1),
(9,'SYSTEM','Konfigurasi Sistem',NULL,1,9,NULL,'sistem','fas fa-cogs',1),
(81,'PELAYANAN','Pelayanan','pelayanan',2,81,8,'pelayanan','fas fa-house-user',1),
(82,'LOKET','Loket','loket',2,82,8,'loket','fas fa-chalkboard-teacher',1),
(93,'USER-ADMIN','Administrator','s_user',2,93,9,'s_user','fas fa-users-cog',1),
(94,'MENU','Daftar Menu','s_menu',2,94,9,'s_menu','fas fa-th-list',1),
(95,'GROUP','Grup Menu','s_group',2,95,9,'s_group','fas fa-users',1);

/*Table structure for table `t_antrian` */

DROP TABLE IF EXISTS `t_antrian`;

CREATE TABLE `t_antrian` (
  `int_antrian_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_pelayanan_id` int(11) DEFAULT NULL,
  `var_no_antrian` varchar(10) DEFAULT NULL,
  `var_nik` varchar(50) DEFAULT NULL,
  `int_status_antrian` int(11) DEFAULT 0 COMMENT 'ref  tabel m_status_antrian',
  `is_panggilan_aktif` tinyint(4) DEFAULT 0,
  `dt_antrian` datetime(6) DEFAULT NULL,
  `dt_selesai_antrian` datetime(6) DEFAULT NULL,
  `is_success_display_led` int(11) DEFAULT 0 COMMENT '0= belum di display, 1 = success, 2 = gagal',
  `int_loket_id` int(11) DEFAULT 0,
  `int_loket_terusan_id` int(11) DEFAULT 0,
  `created_at` datetime(6) DEFAULT current_timestamp(6),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_antrian_id`) USING BTREE,
  KEY `int_antrian_id` (`int_antrian_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_antrian` */

insert  into `t_antrian`(`int_antrian_id`,`int_pelayanan_id`,`var_no_antrian`,`var_nik`,`int_status_antrian`,`is_panggilan_aktif`,`dt_antrian`,`dt_selesai_antrian`,`is_success_display_led`,`int_loket_id`,`int_loket_terusan_id`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,1,'A001','3520102202910001',1,0,'2021-07-18 14:42:36.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-18 14:42:38.876102',NULL,NULL,NULL,NULL,NULL),
(2,1,'A002','',1,0,'2021-07-18 21:42:23.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-18 21:42:23.327697',NULL,NULL,NULL,NULL,NULL),
(3,2,'B001','',1,0,'2021-07-18 21:42:42.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-18 21:42:42.877190',NULL,NULL,NULL,NULL,NULL),
(4,3,'C001','3589369384893829',1,0,'2021-07-18 21:48:33.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-18 21:48:33.382638',NULL,NULL,NULL,NULL,NULL),
(5,3,'C002','',1,0,'2021-07-18 21:48:40.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-18 21:48:40.131629',NULL,NULL,NULL,NULL,NULL),
(6,1,'A001','',1,0,'2021-07-23 23:03:49.000000','2021-08-12 13:15:12.000000',1,0,0,'2021-07-23 23:03:49.809699',NULL,NULL,NULL,NULL,NULL),
(7,1,'A002','',1,0,'2021-07-23 23:03:53.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-23 23:03:53.685616',NULL,NULL,NULL,NULL,NULL),
(8,2,'B001','',1,0,'2021-07-23 23:03:56.000000','2021-08-12 13:15:12.000000',1,0,0,'2021-07-23 23:03:56.218889',NULL,NULL,NULL,NULL,NULL),
(9,1,'A001','',1,0,'2021-07-27 12:01:34.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-27 12:01:34.993618',NULL,NULL,NULL,NULL,NULL),
(10,1,'A002','',1,0,'2021-07-27 12:01:37.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-27 12:01:37.652521',NULL,NULL,NULL,NULL,NULL),
(11,2,'B001','',1,0,'2021-07-27 12:01:39.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-27 12:01:39.977453',NULL,NULL,NULL,NULL,NULL),
(12,3,'C001','',1,0,'2021-07-27 12:01:42.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-07-27 12:01:42.737697',NULL,NULL,NULL,NULL,NULL),
(13,1,'A001','',1,0,'2021-08-07 15:21:51.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-07 15:21:51.751440',NULL,NULL,NULL,NULL,NULL),
(14,1,'A002','',1,0,'2021-08-07 15:21:53.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-07 15:21:53.984720',NULL,NULL,NULL,NULL,NULL),
(15,1,'A003','',1,0,'2021-08-07 15:21:56.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-07 15:21:56.153730',NULL,NULL,NULL,NULL,NULL),
(16,1,'A004','',1,0,'2021-08-07 15:21:59.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-07 15:21:59.898844',NULL,NULL,NULL,NULL,NULL),
(17,2,'B001','',1,0,'2021-08-07 15:22:04.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-07 15:22:04.314796',NULL,NULL,NULL,NULL,NULL),
(18,2,'B002','',1,0,'2021-08-07 15:22:07.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-07 15:22:07.680234',NULL,NULL,NULL,NULL,NULL),
(19,1,'A001','',1,0,'2021-08-12 13:12:12.000000','2021-08-12 22:14:38.000000',0,0,2,'2021-08-12 13:12:12.572214',NULL,NULL,NULL,NULL,NULL),
(20,1,'A002','',0,0,'2021-08-12 13:12:16.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-12 13:12:16.776825',NULL,NULL,NULL,NULL,NULL),
(21,1,'A003','',0,0,'2021-08-12 13:12:18.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-12 13:12:18.978554',NULL,NULL,NULL,NULL,NULL),
(22,2,'B001','',0,0,'2021-08-12 13:12:21.000000','2021-08-12 13:15:12.000000',0,0,0,'2021-08-12 13:12:21.698399',NULL,NULL,NULL,NULL,NULL),
(23,3,'C001','',0,0,'2021-08-12 21:55:54.000000',NULL,0,0,0,'2021-08-12 21:55:54.253146',NULL,NULL,NULL,NULL,NULL),
(24,3,'C002','',0,0,'2021-08-12 21:55:56.000000',NULL,0,0,0,'2021-08-12 21:55:56.694977',NULL,NULL,NULL,NULL,NULL),
(25,4,'D001','',0,0,'2021-08-12 21:55:59.000000',NULL,0,0,0,'2021-08-12 21:55:59.196758',NULL,NULL,NULL,NULL,NULL),
(26,2,'B002','',0,0,'2021-08-12 21:56:12.000000',NULL,0,0,0,'2021-08-12 21:56:12.021243',NULL,NULL,NULL,NULL,NULL),
(27,1,'A001','',0,0,'2021-08-13 11:04:30.000000',NULL,0,0,0,'2021-08-13 11:04:30.874136',NULL,NULL,NULL,NULL,NULL),
(28,1,'A002','',0,0,'2021-08-13 11:13:25.000000',NULL,0,0,0,'2021-08-13 11:13:25.224832',NULL,NULL,NULL,NULL,NULL),
(29,1,'A003','',0,0,'2021-08-13 11:14:04.000000',NULL,0,0,0,'2021-08-13 11:14:04.628806',NULL,NULL,NULL,NULL,NULL),
(30,1,'A004','',0,0,'2021-08-13 11:14:35.000000',NULL,0,0,0,'2021-08-13 11:14:35.280244',NULL,NULL,NULL,NULL,NULL),
(31,1,'A005','',0,0,'2021-08-13 11:15:30.000000',NULL,0,0,0,'2021-08-13 11:15:30.416691',NULL,NULL,NULL,NULL,NULL),
(32,1,'A006','',0,0,'2021-08-13 11:15:50.000000',NULL,0,0,0,'2021-08-13 11:15:50.731136',NULL,NULL,NULL,NULL,NULL),
(33,1,'A007','',0,0,'2021-08-13 11:16:30.000000',NULL,0,0,0,'2021-08-13 11:16:30.634913',NULL,NULL,NULL,NULL,NULL),
(34,1,'A008','',0,0,'2021-08-13 11:19:35.000000',NULL,0,0,0,'2021-08-13 11:19:35.687301',NULL,NULL,NULL,NULL,NULL),
(35,1,'A009','',0,0,'2021-08-13 11:20:19.000000',NULL,0,0,0,'2021-08-13 11:20:19.840860',NULL,NULL,NULL,NULL,NULL),
(36,1,'A010','',0,0,'2021-08-13 11:20:36.000000',NULL,0,0,0,'2021-08-13 11:20:36.532435',NULL,NULL,NULL,NULL,NULL),
(37,1,'A011','',0,0,'2021-08-13 11:21:36.000000',NULL,0,0,0,'2021-08-13 11:21:36.278521',NULL,NULL,NULL,NULL,NULL),
(38,1,'A012','',0,0,'2021-08-13 11:22:02.000000',NULL,0,0,0,'2021-08-13 11:22:02.357106',NULL,NULL,NULL,NULL,NULL),
(39,1,'A013','',0,0,'2021-08-13 11:22:27.000000',NULL,0,0,0,'2021-08-13 11:22:27.506800',NULL,NULL,NULL,NULL,NULL),
(40,1,'A014','',0,0,'2021-08-13 11:22:31.000000',NULL,0,0,0,'2021-08-13 11:22:31.299427',NULL,NULL,NULL,NULL,NULL),
(41,1,'A015','',0,0,'2021-08-13 11:22:38.000000',NULL,0,0,0,'2021-08-13 11:22:38.261295',NULL,NULL,NULL,NULL,NULL),
(42,1,'A016','',0,0,'2021-08-13 11:23:45.000000',NULL,0,0,0,'2021-08-13 11:23:45.446730',NULL,NULL,NULL,NULL,NULL),
(43,1,'A017','',0,0,'2021-08-13 11:24:11.000000',NULL,0,0,0,'2021-08-13 11:24:11.270329',NULL,NULL,NULL,NULL,NULL),
(44,1,'A018','',0,0,'2021-08-13 11:24:17.000000',NULL,0,0,0,'2021-08-13 11:24:17.742813',NULL,NULL,NULL,NULL,NULL),
(45,1,'A019','',0,0,'2021-08-13 11:24:46.000000',NULL,0,0,0,'2021-08-13 11:24:46.777003',NULL,NULL,NULL,NULL,NULL),
(46,1,'A020','',0,0,'2021-08-13 11:25:25.000000',NULL,0,0,0,'2021-08-13 11:25:25.840927',NULL,NULL,NULL,NULL,NULL),
(47,1,'A021','',0,0,'2021-08-13 11:27:21.000000',NULL,0,0,0,'2021-08-13 11:27:21.857176',NULL,NULL,NULL,NULL,NULL),
(48,1,'A022','',0,0,'2021-08-13 11:27:32.000000',NULL,0,0,0,'2021-08-13 11:27:32.087375',NULL,NULL,NULL,NULL,NULL),
(49,1,'A023','',0,0,'2021-08-13 11:28:34.000000',NULL,0,0,0,'2021-08-13 11:28:34.509651',NULL,NULL,NULL,NULL,NULL),
(50,1,'A024','',0,0,'2021-08-13 11:28:59.000000',NULL,0,0,0,'2021-08-13 11:28:59.122325',NULL,NULL,NULL,NULL,NULL),
(51,1,'A025','',0,0,'2021-08-13 11:30:01.000000',NULL,0,0,0,'2021-08-13 11:30:01.507111',NULL,NULL,NULL,NULL,NULL),
(52,1,'A026','',0,0,'2021-08-13 11:30:53.000000',NULL,0,0,0,'2021-08-13 11:30:53.172390',NULL,NULL,NULL,NULL,NULL),
(53,1,'A027','',0,0,'2021-08-13 11:31:25.000000',NULL,0,0,0,'2021-08-13 11:31:25.363455',NULL,NULL,NULL,NULL,NULL),
(54,1,'A028','',0,0,'2021-08-13 11:34:19.000000',NULL,0,0,0,'2021-08-13 11:34:19.008991',NULL,NULL,NULL,NULL,NULL),
(55,1,'A029','',0,0,'2021-08-13 11:35:06.000000',NULL,0,0,0,'2021-08-13 11:35:06.591146',NULL,NULL,NULL,NULL,NULL),
(56,1,'A030','',0,0,'2021-08-13 11:35:14.000000',NULL,0,0,0,'2021-08-13 11:35:14.899790',NULL,NULL,NULL,NULL,NULL),
(57,1,'A031','',0,0,'2021-08-13 15:28:25.000000',NULL,0,0,0,'2021-08-13 15:28:25.830903',NULL,NULL,NULL,NULL,NULL),
(58,1,'A032','',0,0,'2021-08-13 15:42:10.000000',NULL,0,0,0,'2021-08-13 15:42:10.455274',NULL,NULL,NULL,NULL,NULL),
(59,1,'A001','',0,1,'2021-08-14 08:30:45.000000',NULL,0,1,0,'2021-08-14 08:30:45.712360',NULL,NULL,NULL,NULL,NULL),
(60,1,'A002','',0,0,'2021-08-14 08:30:48.000000',NULL,0,0,0,'2021-08-14 08:30:48.792477',NULL,NULL,NULL,NULL,NULL),
(61,1,'A003','',0,0,'2021-08-14 08:30:51.000000',NULL,0,0,0,'2021-08-14 08:30:51.148378',NULL,NULL,NULL,NULL,NULL),
(62,1,'A001','',1,1,'2021-08-15 21:10:18.000000','2021-08-15 22:14:08.000000',0,2,2,'2021-08-15 21:10:18.288502',NULL,NULL,NULL,NULL,NULL),
(63,3,'C001','',0,1,'2021-08-15 21:10:22.000000',NULL,0,3,0,'2021-08-15 21:10:22.793345',NULL,NULL,NULL,NULL,NULL),
(64,5,'E001','',0,1,'2021-08-15 21:10:26.000000',NULL,0,6,0,'2021-08-15 21:10:26.043358',NULL,NULL,NULL,NULL,NULL),
(65,6,'F001','',0,0,'2021-08-15 21:10:28.000000',NULL,0,0,0,'2021-08-15 21:10:28.380115',NULL,NULL,NULL,NULL,NULL),
(66,1,'A002','',0,0,'2021-08-15 22:13:35.000000',NULL,0,1,0,'2021-08-15 22:13:35.399329',NULL,NULL,NULL,NULL,NULL),
(67,3,'C002','',0,0,'2021-08-15 22:13:40.000000',NULL,0,0,0,'2021-08-15 22:13:40.868736',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_panggilan_antrian` */

DROP TABLE IF EXISTS `t_panggilan_antrian`;

CREATE TABLE `t_panggilan_antrian` (
  `int_panggilan_antrian_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_antrian_id` int(11) DEFAULT NULL,
  `int_pelayanan_id` int(11) DEFAULT NULL,
  `int_loket_id` int(11) DEFAULT NULL,
  `var_no_antrian` varchar(20) DEFAULT NULL,
  `int_status_panggilan` int(5) DEFAULT NULL COMMENT '0 = belum dipanggil,\r\n1 = sudah dipanggil',
  `dt_panggilan` datetime(6) DEFAULT current_timestamp(6),
  `created_at` datetime(6) DEFAULT current_timestamp(6),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_panggilan_antrian_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_panggilan_antrian` */

insert  into `t_panggilan_antrian`(`int_panggilan_antrian_id`,`int_antrian_id`,`int_pelayanan_id`,`int_loket_id`,`var_no_antrian`,`int_status_panggilan`,`dt_panggilan`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,1,1,1,'A001',1,'2021-07-18 23:30:51.000000','2021-07-18 23:30:53.104813',NULL,NULL,NULL,NULL,NULL),
(2,6,1,1,'A001',0,NULL,'2021-07-23 23:05:05.869653',NULL,NULL,NULL,NULL,NULL),
(3,8,2,2,'B001',0,'2021-07-23 23:19:05.604619','2021-07-23 23:19:05.604619',NULL,NULL,NULL,NULL,NULL),
(4,9,1,1,'A001',1,'2021-07-27 12:46:52.201149','2021-07-27 12:46:52.201149',NULL,NULL,NULL,NULL,NULL),
(5,19,1,2,'A001',1,'2021-08-12 22:18:00.000000','2021-08-12 13:13:46.000000',NULL,NULL,NULL,NULL,NULL),
(6,59,1,1,'A001',0,'2021-08-14 08:32:06.000000','2021-08-14 08:32:06.000000',NULL,NULL,NULL,NULL,NULL),
(7,62,1,2,'A001',0,'2021-08-15 21:11:24.000000','2021-08-15 21:11:24.000000',NULL,NULL,NULL,NULL,NULL),
(8,63,3,3,'C001',0,'2021-08-15 21:24:37.000000','2021-08-15 21:24:37.000000',NULL,NULL,NULL,NULL,NULL),
(9,64,5,6,'E001',0,'2021-08-15 21:27:47.000000','2021-08-15 21:27:47.000000',NULL,NULL,NULL,NULL,NULL),
(10,66,1,1,'A002',0,'2021-08-15 22:14:13.000000','2021-08-15 22:14:13.000000',NULL,NULL,NULL,NULL,NULL),
(11,62,NULL,2,NULL,0,'2021-08-15 22:14:32.000000','2021-08-15 22:14:32.000000',NULL,NULL,NULL,NULL,NULL),
(12,62,NULL,2,NULL,0,'2021-08-15 22:14:51.000000','2021-08-15 22:14:51.000000',NULL,NULL,NULL,NULL,NULL),
(13,62,NULL,2,NULL,0,'2021-08-15 22:15:02.000000','2021-08-15 22:15:02.000000',NULL,NULL,NULL,NULL,NULL),
(14,62,NULL,2,NULL,0,'2021-08-15 22:15:21.000000','2021-08-15 22:15:21.000000',NULL,NULL,NULL,NULL,NULL),
(15,62,NULL,2,NULL,0,'2021-08-15 22:24:36.000000','2021-08-15 22:24:36.000000',NULL,NULL,NULL,NULL,NULL);

/* Procedure structure for procedure `generate_no_antrian` */

/*!50003 DROP PROCEDURE IF EXISTS  `generate_no_antrian` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `generate_no_antrian`(IN in_int_pelayanan_id INT,
	IN in_var_nik VARCHAR(20))
BEGIN
	DECLARE d_count_antrian INT;
	DECLARE d_nomor_antrian_number INT;
	DECLARE d_nomor_antrian_tmp VARCHAR(10);
	DECLARE d_prefix VARCHAR(10);
	DECLARE d_nomor_antrian VARCHAR(10);
	DECLARE d_var_pelayanan VARCHAR(100);
	-- Start Generate No Antrian
	SET d_count_antrian = (SELECT MAX(CAST(SUBSTRING(var_no_antrian, -3) AS UNSIGNED)) FROM t_antrian WHERE int_pelayanan_id = in_int_pelayanan_id and DATE(dt_antrian) = DATE(NOW()));
	IF(d_count_antrian = 0 OR d_count_antrian IS NULL) THEN
		SET d_nomor_antrian_number = 1;
	ELSE
		SET d_nomor_antrian_number = d_count_antrian + 1;
	END IF;
	SET d_nomor_antrian_tmp = LPAD(d_nomor_antrian_number,3,"000");
	SET d_prefix 						= (SELECT var_prefix FROM m_pelayanan WHERE int_pelayanan_id = in_int_pelayanan_id limit 1);
	SET d_nomor_antrian 		= CONCAT(d_prefix,d_nomor_antrian_tmp);
	-- End Generate No Antrian
	SET d_var_pelayanan = (SELECT var_pelayanan FROM m_pelayanan WHERE int_pelayanan_id = in_int_pelayanan_id limit 1);
INSERT INTO t_antrian (
	int_pelayanan_id,
	var_no_antrian,
	var_nik,
	int_status_antrian,
	is_panggilan_aktif,
	dt_antrian
)
VALUES
	(
		in_int_pelayanan_id,
		d_nomor_antrian,
		in_var_nik,
		0,
		0,
		NOW()
	);
	SELECT d_nomor_antrian as var_no_antrian ,d_var_pelayanan AS var_pelayanan, NOW() as dt_antrian;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_data_antrian_perloket` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_data_antrian_perloket` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `get_data_antrian_perloket`(IN in_int_loket_id INT,
	IN in_dt_antrian DATE)
BEGIN
	
	DECLARE is_loket_lanjutan INT;
	SET is_loket_lanjutan = (SELECT COUNT(int_loket_terusan_id) from m_loket WHERE int_loket_terusan_id = in_int_loket_id);

IF(in_int_loket_id = 0 OR in_int_loket_id = '') THEN
	SELECT
	 t.int_antrian_id,
	 t.int_pelayanan_id,
	 p.var_pelayanan,
	 t.var_no_antrian,
	 t.var_nik,
	 t.dt_antrian,
	 st.var_status_antrian,
	 t.int_status_antrian,
	 m.var_ip_led,
	 m.int_loket_id,
	 m.var_loket,
	 m.int_loket_terusan_id
FROM
	t_antrian t
	JOIN m_loket m ON ( m.int_loket_id = in_int_loket_id )
	JOIN m_pelayanan p ON ( p.int_pelayanan_id = t.int_pelayanan_id ) 
	LEFT JOIN m_status_antrian st ON (st.int_status_antrian = t.int_status_antrian)
WHERE
 t.int_status_antrian = 0 AND DATE(t.dt_antrian) = DATE(in_dt_antrian);
ELSE
	-- jika loket tersebut merupakan loket lanjutan, maka tampilkan data antrian dari loket asal yang sudah selesai dilayani
	IF(is_loket_lanjutan > 0) THEN
			SELECT
				 t.int_antrian_id,
				 t.int_pelayanan_id,
				 p.var_pelayanan,
				 t.var_no_antrian,
				 t.var_nik,
				 t.dt_antrian,
				 st.var_status_antrian,
				 t.int_status_antrian,
				 m.var_ip_led,
				 m.int_loket_id,
				 m.var_loket,
				 m.int_loket_terusan_id
			FROM
				t_antrian t
				JOIN m_loket m ON ( m.int_loket_id = in_int_loket_id )
				JOIN m_pelayanan p ON ( p.int_pelayanan_id = t.int_pelayanan_id ) 
				LEFT JOIN m_status_antrian st ON (st.int_status_antrian = t.int_status_antrian)
			WHERE
				t.int_status_antrian = 1 
				AND t.int_loket_terusan_id = in_int_loket_id
				AND DATE(t.dt_antrian) = DATE(in_dt_antrian);
	ELSE
			SELECT
				 t.int_antrian_id,
				 t.int_pelayanan_id,
				 p.var_pelayanan,
				 t.var_no_antrian,
				 t.var_nik,
				 t.dt_antrian,
				 st.var_status_antrian,
				 t.int_status_antrian,
				 m.var_ip_led,
				 m.int_loket_id,
				 m.var_loket,
				 m.int_loket_terusan_id
			FROM
				t_antrian t
				JOIN m_loket m ON ( m.int_loket_id = in_int_loket_id )
				JOIN m_pelayanan p ON ( p.int_pelayanan_id = t.int_pelayanan_id ) 
				LEFT JOIN m_status_antrian st ON (st.int_status_antrian = t.int_status_antrian)
			WHERE
				t.int_status_antrian = 0 AND t.int_pelayanan_id IN (SELECT int_pelayanan_id FROM m_loket WHERE int_loket_id = in_int_loket_id)
				AND DATE(t.dt_antrian) = DATE(in_dt_antrian);
	END IF;
END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_monitoring_antrian_loket` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_monitoring_antrian_loket` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `get_monitoring_antrian_loket`(IN in_dt_antrian DATE)
BEGIN
 SELECT
	l.int_loket_id,
	l.int_pelayanan_id,
	l.var_loket,
	l.var_color,
	l.var_icon,
	IFNULL((SELECT p.var_pelayanan FROM m_pelayanan p WHERE p.int_pelayanan_id = l.int_pelayanan_id limit 1),'') as var_pelayanan,
	IFNULL((SELECT t.var_no_antrian FROM t_antrian t WHERE t.is_panggilan_aktif = 1 AND t.int_loket_id = l.int_loket_id AND DATE( t.dt_antrian ) = DATE(in_dt_antrian) limit 1),'') as var_no_antrian
FROM
	m_loket l 
	WHERE l.is_aktif = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_monitoring_antrian_pelayanan` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_monitoring_antrian_pelayanan` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `get_monitoring_antrian_pelayanan`(IN in_dt_antrian DATE)
BEGIN
SELECT
	p.int_pelayanan_id,
	p.var_pelayanan,
	p.var_color,
	p.var_icon,
	IFNULL(l.var_loket,'') as var_loket,
	IFNULL( t.var_no_antrian, 0 ) AS var_no_antrian 
FROM
	m_pelayanan p
	LEFT JOIN t_antrian t ON (
		t.int_pelayanan_id = p.int_pelayanan_id 
		AND t.is_panggilan_aktif = 1 
		AND DATE( t.dt_antrian ) = DATE(in_dt_antrian))
	LEFT JOIN t_panggilan_antrian tp ON ( tp.int_antrian_id = t.int_antrian_id )
	LEFT JOIN m_loket l ON ( l.int_loket_id = tp.int_loket_id ) 
WHERE
	p.is_aktif = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_no_antrian_display_to_led` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_no_antrian_display_to_led` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `get_no_antrian_display_to_led`()
BEGIN
SELECT
	t.int_antrian_id,
	t.int_pelayanan_id,
	t.var_no_antrian,
	tp.int_loket_id,
	l.var_ip_led 
FROM
	t_antrian t
	JOIN t_panggilan_antrian tp ON t.int_antrian_id = tp.int_antrian_id
	JOIN m_loket l ON l.int_loket_id = tp.int_loket_id 
WHERE
	t.is_panggilan_aktif = 1 
	AND tp.int_status_panggilan = 0 
	AND DATE( t.dt_antrian ) = DATE(NOW())
	AND t.is_success_display_led = 0 limit 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `loket_panggil_antrian` */

/*!50003 DROP PROCEDURE IF EXISTS  `loket_panggil_antrian` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `loket_panggil_antrian`(IN in_int_antrian_id INT,
	IN in_var_no_antrian VARCHAR(50),
	IN in_int_pelayanan_id INT,
	IN in_int_loket_id INT,
	IN in_dt_antrian DATE)
BEGIN
	IF NOT EXISTS(SELECT int_antrian_id FROM t_panggilan_antrian WHERE int_antrian_id = in_int_antrian_id AND DATE(created_at) = DATE(in_dt_antrian)) THEN
			INSERT INTO t_panggilan_antrian 
						(int_antrian_id,
						int_pelayanan_id,
						int_loket_id,
						var_no_antrian,
						int_status_panggilan,
						dt_panggilan,
						created_at )
			VALUES
						(in_int_antrian_id,
						in_int_pelayanan_id,
						in_int_loket_id,
						in_var_no_antrian,
						0,
						NOW(),
						NOW()
						);
				
				UPDATE t_antrian SET is_panggilan_aktif = 0
				WHERE int_antrian_id IN (SELECT int_antrian_id FROM t_panggilan_antrian WHERE int_loket_id = in_int_loket_id);
				
				UPDATE t_antrian SET is_panggilan_aktif = 1, int_loket_id = in_int_loket_id
				WHERE int_antrian_id = in_int_antrian_id;
				
				SELECT 1 as sql_code, 'success' as var_info;
			
	ELSE
			
				UPDATE t_antrian SET is_panggilan_aktif = 0
				WHERE int_antrian_id IN (SELECT int_antrian_id FROM t_panggilan_antrian WHERE int_loket_id = in_int_loket_id);
				
				UPDATE t_antrian SET is_panggilan_aktif = 1, int_loket_id = in_int_loket_id
				WHERE int_antrian_id = in_int_antrian_id;
			
				UPDATE t_panggilan_antrian SET int_status_panggilan = 0, int_loket_id = in_int_loket_id
				WHERE int_antrian_id = in_int_antrian_id;
				
			SELECT 1 as sql_code, 'success' as var_info;
	END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `loket_set_antrian_selesai` */

/*!50003 DROP PROCEDURE IF EXISTS  `loket_set_antrian_selesai` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `loket_set_antrian_selesai`(IN in_int_antrian_id INT,
		IN in_int_loket_id INT,
		IN in_int_loket_terusan_id INT)
BEGIN

	DECLARE is_loket_lanjutan INT;
	SET is_loket_lanjutan = (SELECT COUNT(int_loket_terusan_id) from m_loket WHERE int_loket_terusan_id = in_int_loket_id);
	
	-- jika loket lanjutan, update status menjadi 3 (selesai dilayani oleh loket lanjutan)
	IF(is_loket_lanjutan > 0) THEN
			UPDATE t_antrian SET int_status_antrian = 3, dt_selesai_antrian = NOW()
			WHERE int_antrian_id = in_int_antrian_id;
			
			SELECT 1 as sql_code, 'success' as var_info;
	ELSE
			UPDATE t_antrian SET int_status_antrian = 1, dt_selesai_antrian = NOW(), int_loket_terusan_id = in_int_loket_terusan_id
			WHERE int_antrian_id = in_int_antrian_id;
			
			SELECT 1 as sql_code, 'success' as var_info;
	END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `mapper_get_panggil_suara_antrian` */

/*!50003 DROP PROCEDURE IF EXISTS  `mapper_get_panggil_suara_antrian` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `mapper_get_panggil_suara_antrian`()
BEGIN
		SELECT tp.var_no_antrian,
		(SELECT l.var_suara_antrian FROM m_loket l WHERE l.int_loket_id = tp.int_loket_id limit 1) as var_suara_loket,
		tp.int_antrian_id
		 FROM t_panggilan_antrian tp 
		 WHERE int_status_panggilan = 0 
		 and DATE(created_at) = DATE(NOW()) ORDER BY created_at ASC limit 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `mapper_upd_antrian_selesai_dipanggil` */

/*!50003 DROP PROCEDURE IF EXISTS  `mapper_upd_antrian_selesai_dipanggil` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `mapper_upd_antrian_selesai_dipanggil`(IN in_id_antrian INT)
BEGIN
		UPDATE t_panggilan_antrian SET int_status_panggilan = 1, dt_panggilan = NOW()
		WHERE int_antrian_id = in_id_antrian;
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_no_antrian_display_to_led` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_no_antrian_display_to_led` */;

DELIMITER $$

/*!50003 CREATE PROCEDURE `update_no_antrian_display_to_led`(IN in_int_antrian_id INT,
	IN in_int_status INT)
BEGIN
UPDATE t_antrian SET is_success_display_led = in_int_status WHERE int_antrian_id = in_int_antrian_id;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
